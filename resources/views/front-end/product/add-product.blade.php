
@extends('front-end.master')
@section('body')

<style>
        .row{

            padding-right: -450px;
            padding-left: 500px;
            margin-top: 100px;
        }


        .panel-heading {
            padding-right: 150px;
        }



        .message{
            padding-right: 150px;
        }


    </style>




<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="message">
            <h3 class="text-center text-success"> {{ Session::get('message')}} </h3>
            </div>
         <div class="panel-heading">
                <h4 class="text-center text-success"> Add Product From  </h4>
            </div>
            <div class="panel-body">
                <form action="{{route('new-product')}}  " method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Name</label>
                        <div class="col-md-9">
                            <input type="text" name="product_name" class="form-control"/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Product Type</label>
                        <div class="col-md-9">
                            <input type="text" name="product_type" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Product Group</label>
                        <div class="col-md-9">
                            <input type="text" name="product_group" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Product Quantity</label>
                        <div class="col-md-9">
                            <input type="text" name="product_quantity" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Branch</label>
                        <div class="col-md-9">
                            <input type="text" name="branch" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Product Price</label>
                        <div class="col-md-9">
                            <input type="text" name="selling_price" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Product Cost</label>
                        <div class="col-md-9">
                            <input type="text" name="selling_cost" class="form-control"/>
                        </div>
                    </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Publication status</label>
                        <div class="col-md-9 radio">
                            <label><input type="radio" name="publication_status" value="1"/> Stock </label>
                            <label><input type="radio" name="publication_status" value="0"/> Out of Stock </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <input type="submit" name="btn" value="Save Category Info" class="btn btn-success btn-block"/>
                        </div>
                    </div>

            </div>
        </div>
    </div>


@endsection
