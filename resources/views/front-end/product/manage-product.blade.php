@extends('front-end.master')

<style>
        .row{
            padding-right: -500px;
            padding-left: 250px;
            margin-top: 110px;
        }


        .panel-heading {
            padding-right: 150px;
        }


        .panel-body{
            margin-top: 20px;
        }

        .message{
            padding-right: 150px;
        }
        .search-content{
            padding-right: 200px;
        }



    </style>
@section('body')




<div class="row">
    <div class="col-md-12 ">
        <div class="panel panel-default">
            <div class="message">
            <h3 class="text-center text-success"> {{ Session::get('message')}} </h3>
            </div>
            <div class="search-content">
                <form action="{{route('search')}}", method="get">
                    <div class="form-group">

                        <input type="text" class="ti ti-search magnifier" placeholder="Search Here", id="autocomplete-ajax" autofocus="autofocus">
                   <button>search</button>
                    </div>
                </form>
            </div>
         <div class="panel-heading">
                <h4 class="text-center text-success"> Total Stock  </h4>
            </div>
            <div class="panel-body">


                <table class="table table-bordered">
                    <tr>
                        <th>Sl no</th>
                        <th>Product Name</th>
                        <th>Product Type</th>
                        <th>Product Group</th>
                        <th>Product Quantity</th>
                        <th>Branch</th>
                        <th>Selling Price</th>
                        <th>Selling Cost</th>
                        <th>Publication status</th>

                        <th>Action</th>

                    </tr>

                    @php($i=1)

                    @foreach($products as $product)

                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $product->product_name }}</td>
                        <td>{{ $product->product_type }}</td>
                        <td>{{ $product->product_group }}</td>
                        <td>{{ $product->product_quantity }}</td>
                        <td>{{ $product->branch }}</td>
                        <td>{{ $product->selling_price }}</td>
                        <td>{{ $product->selling_cost }}</td>
                        <td>{{ $product->publication_status == 1 ? 'Stock' : 'Stock Out' }}</td>
                        <td>

                            @if($product->Publication_status == 0)
                                <a href="{{ route('stockout-product',[$product])}}" class="btn btn-warning btn-xs">
                                    <img style="height:20px" src="{{ asset('admin') }}/img/icon/upload.svg"/> </a>
                            @else
                                <a href=" {{ route('stock-product',[$product])}} " class="btn btn-info btn-xs">
                                    <img style="height:20px" src="{{ asset('admin') }}/img/icon/upload.svg"/> </a>



                            @endif


                            <a href=" {{ route('edit-product',[$product])}} " class="btn btn-success btn-xs">
                            <i class="fas fa-edit"></i>
                            </a>

                            <a onclick="return confirm('are you sure delete this data')" href=" {{ route('delete-product',[$product])}} " class="btn btn-danger btn-xs">
                            <img style="height:20px" src="{{ asset('admin') }}/img/icon/trash-can.svg"/>
                            </a>
                        </td>
                    </tr>

                    @endforeach
                </table>

            </div>
        </div>
    </div>
</div>

@endsection
