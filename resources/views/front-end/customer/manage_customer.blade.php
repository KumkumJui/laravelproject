@extends('front-end.master')

<style>
    .row{
        padding-right: -500px;
        padding-left: 260px;
        margin-top: 60px;
    }


    .panel-heading {
        padding-right: 150px;
    }


    .panel-body{
        margin-top: 20px;
    }

    .message{
        padding-right: 150px;
    }



</style>


@section('body')

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="message">
                    <h3 class="text-center text-success"> {{ Session::get('message')}} </h3>
                </div>
                <div class="panel-heading">
                    <h4 class="text-center text-success"> Customer From  </h4>
                </div>
                <div class="panel-body">


                    <table class="table table-bordered">
                        <tr>
                            <th>Sl no</th>
                            <th>Customer Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone No</th>


                        </tr>

                        @php($i=1)

                        @foreach($customers as $customer)

                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $customer->customer_name }}</td>
                                <td>{{ $customer->email }}</td>
                                <td>{{ $customer->address }}</td>
                                <td>{{ $customer->phone_no }}</td>


                            </tr>

                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection
