@extends('front-end.master')


@section('body')

    <style>
        .row{

            padding-right: -450px;
            padding-left: 500px;
            margin-top: 100px;
        }


        .panel-heading {
            padding-right: 150px;
        }



        .message{
            padding-right: 150px;
        }


    </style>




    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="message">
                    <h3 class="text-center text-success"> {{ Session::get('message')}} </h3>
                </div>
                <div class="panel-heading">
                    <h4 class="text-center text-success"> Add Customer From  </h4>
                </div>
                <div class="panel-body">
                    <form action="{{route('new-customer')}}  " method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Name</label>
                            <div class="col-md-9">
                                <input type="text" name="customer_name" class="form-control"/>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input type="text" name="email" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Address</label>
                            <div class="col-md-9">
                                <input type="text" name="address" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Phone No</label>
                            <div class="col-md-9">
                                <input type="text" name="phone_no" class="form-control"/>
                            </div>
                        </div>







                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <input type="submit" name="btn" value="Save Customer Info" class="btn btn-success btn-block"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection


