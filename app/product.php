<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $fillable = ['product_name','product_type', 'product_group','product_quantity','branch','selling_price','selling_cost','publication_status'];
}
