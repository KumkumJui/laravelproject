<?php

namespace App\Http\Controllers;
use App\User;
use App\Customer;
use App\Product;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {

        $total_cost = Product::all()->sum('selling_cost');
        $total_sales = Product::all()->avg('selling_cost');
        $previous_customers = Customer::all();
        $users = User::all();
        $customers = Customer::all();
        $publication_status = Product::all()->sum('publication_status');
        return view('front-end.home.home', compact('users', 'customers', 'previous_customers', 'publication_status','total_cost','total_sales'));









    }






}
