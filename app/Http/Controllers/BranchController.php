<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function Uttara()
    {
        return view('front-end.branch.uttara');
    }
    public function Banani()
    {
        return view('front-end.branch.banani');
    }
    public function Dhanmondi()
    {
        return view('front-end.branch.dhanmondi');
    }
    public function Gulshan()
    {
        return view('front-end.branch.gulshan');
    }
}
