<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
/*
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('customer');
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('front-end.product.add-product');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveProduct( Request $request)

    {
        Product::create($request->all());

        /*
        $product = new Product();
        $product->product_name = $request->product_name;
        $product->product_type = $request->product_type;
        $product->product_group = $request->product_group;
        $product->product_quantity = $request->product_quantity;
        $product->selling_price  = $request->selling_price;
        $product->selling_cost = $request->selling_cost;
        $product->publication_status = $request->publication_status;

        $product->save();
*/

        return redirect('/product/add')->with('message', 'product Save Successfully');
    }
    public function searchProduct( Request $request)

    {
        $product= $request->product;
        $data = DB::table('products')->where('products.product_name', $product)->get();
        return view ('front-end.product.manage_product', ['data'=>$data, 'productByUser'=>$product]);

    }
    public function search(Request $request){
        $searchData = $request->searchData;
        $data = DB:: table('products')->where('product_name', 'like', '%')->get();
    }

    public function manageProduct() {
        $products = Product::all();

        return view ('front-end.product.manage-product', ['products'=>$products]);
    }



    public function stockout($id){
        $product = Product::find($id);
        $product->publication_status = 0;
        $product->save();

        return redirect('/product/manage')->with('message', 'Product Stock Out');
     }


     public function stock($id){
        $product = Product::find($id);
        $product->publication_status = 1;
        $product->save();

        return redirect('/product/manage')->with('message', 'Product Stock');
     }


     public function editProduct($id){
        $products = Product::findOrFail($id);
        return view('admin.product.edit-product', ['products' =>$products] );


     }


     public function updateProduct($id,Request $request ){
        $products = Product::find($id);

        //return $request;

         $products->product_name= $request->product_name;
         $products->product_type= $request->product_type;
         $products->product_group= $request->product_group;
         $products->product_quantity= $request->product_quantity;
         $products->branch= $request->branch;
         $products->selling_price= $request->selling_price;
         $products->selling_cost= $request->selling_cost;
         $products->publication_status= $request->publication_status;
         $products->save();

        return redirect('/product/manage')->with('message', 'Product Update');

     }



     public function deleteProduct($id, Request $request) {
        $product = Product::find($id);
        $product->delete();

        return redirect('/product/manage')->with('message', 'Product delete');
     }
     }
