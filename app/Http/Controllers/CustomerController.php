<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class CustomerController extends Controller


{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function customers()
    {
        return view('front-end.customer.add_customer');
    }

    public function saveCustomer(Request $request)

    {
        Customer::create($request->all());
        return redirect('/customer/add')->with('message', 'Save Successfully');
    }
    public function manageCustomer() {
        $customers = Customer::all();

        return view ('front-end.customer.manage_customer', ['customers'=>$customers]);
    }
}
