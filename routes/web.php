<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('Uttara', [
    'uses'  => 'BranchController@Uttara',
    'as'    => 'uttara'

]);



Route::get('Dhanmondi', [
    'uses'  => 'BranchController@Dhanmondi',
    'as'    => 'dhanmondi'

]);



Route::get('Gulshan', [
    'uses'  => 'BranchController@Gulshan',
    'as'    => 'gulshan'

]);


Route::get('Banani', [
    'uses'  => 'BranchController@Banani',
    'as'    => 'banani'

]);



Route::get('/product/add', [
    'uses'  => 'ProductController@index',
    'as'    => 'add-product'

]);


Route::get('/product/manage', [
    'uses'  => 'ProductController@manageProduct',
    'as'    => 'manage-product'

]);



Route::post('/product/save', [
    'uses'  => 'ProductController@saveProduct',
    'as'    => 'new-product'

]);
Route::get('/customer/add', [
    'uses' => 'CustomerController@customers',
    'as' => 'add_customer',

]);


Route::get('/customer/manage', [
    'uses' => 'CustomerController@manageCustomer',
    'as' => 'manage_customer'

]);


Route::post('/customer/save', [
    'uses' => 'CustomerController@saveCustomer',
    'as' => 'new-customer'


]);

Route::get('/product/stockout/{id}', [
    'uses'  => 'ProductController@stockout',
    'as'    => 'stockout-product'

]);


Route::get('/product/stock/{id}', [
    'uses'  => 'ProductController@stock',
    'as'    => 'stock-product'

]);

Route::get('/product/edit/{id}', [
    'uses'  => 'ProductController@editProduct',
    'as'    => 'edit-product'

]);


Route::post('/product/update/{id}', [
    'uses'  => 'ProductController@updateProduct',
    'as'    => 'update-product'

]);

Route::get('/product/delete/{id}', [
    'uses'  => 'ProductController@deleteProduct',
    'as'    => 'delete-product'

]);

Route::get('/product/save', [
    'uses'  => 'ProductController@searchProduct',
    'as'    => 'search'
    ]);





Auth::routes();

Route::get('/', 'HomeController@home')->name('home');
